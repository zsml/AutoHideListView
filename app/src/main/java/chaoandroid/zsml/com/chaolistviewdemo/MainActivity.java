package chaoandroid.zsml.com.chaolistviewdemo;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    private RelativeLayout titlebar;

    //ListView
    private ListView mlistview;


    //数据集合
    private List<String> mData;

    //适配器
    private MyAdapter adapter;


    // 最小滑动距离
    private int minInstance;
    // 记录开始位置
    private float mFirstY;
    // 当前位置
    private float mCurrentY;
    // 滑动的两种状态（ 0 向下， 1 向上）
    private int status;
    private boolean mShow = true;
    private ObjectAnimator animator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mlistview = (ListView) findViewById(R.id.lv);
        titlebar = (RelativeLayout) findViewById(R.id.titlebar);

        //1.给listview增加HeaderView
        // 给 ListView 添加一个 HeaderView
        View header = new View(this);
        header.setLayoutParams(new AbsListView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                // 高度根据屏幕不一样，大小不一
                // （此处可以使用 R.dimen.abc_action_bar_default_height_material）
                (int) getResources().getDimension(
                        R.dimen.abc_action_bar_default_height_material)));
        mlistview.addHeaderView(header);

        //2.获取系统认为的最低滑动距离
        minInstance = ViewConfiguration.get(this).getScaledTouchSlop();

        //3.判断滑动事件
        mlistview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mFirstY = event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        mCurrentY = event.getY();
                        if (mCurrentY - mFirstY > minInstance) {
                            status = 0;  // 向下
                        } else {
                            status = 1;  // 向上
                        }
                        if (status == 1) {
                            if (mShow) {
                                toolbarAnim(1);  // 隐藏
                                mShow = false;
                            }
                        } else {
                            if (!mShow) {
                                toolbarAnim(0); // 显示
                                mShow = true;
                            }
                        }
                        break;
                }
                return false;
            }
        });


        //准备数据
        mData = new ArrayList<>();
        for (int i=1;i<=30;i++){
            mData.add("超哥ListView"+i);
        }

        //设置适配器
        adapter = new MyAdapter();
        mlistview.setAdapter(adapter);

    }

    private void toolbarAnim(int i) {
        if (animator != null && animator.isRunning())
            animator.cancel();
        if (i == 0) {
            animator = ObjectAnimator.ofFloat(
                    titlebar,
                    "translationY",
                    titlebar.getTranslationY(),
                    0);
        }else{
            animator = ObjectAnimator.ofFloat(
                    titlebar,
                    "translationY",
                    titlebar.getTranslationY(),
                    -titlebar.getHeight());
            }
        animator.start();
    }


    class MyAdapter extends BaseAdapter{


        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            //判断是否缓存
            if (convertView == null){
                //实例化布局
                convertView = View.inflate(MainActivity.this,R.layout.list_items,null);
                holder = new ViewHolder();
                holder.iv = (ImageView) convertView.findViewById(R.id.iv);
                holder.tv = (TextView) convertView.findViewById(R.id.tv);
                convertView.setTag(holder);

            }else {
                //通过tag找到缓存布局
                holder = (ViewHolder) convertView.getTag();
            }
            //设置显示的视图
            holder.iv.setBackgroundResource(R.drawable.chaoandroid_logo);
            holder.tv.setText(mData.get(position));
            return convertView;
        }
    }

    static class ViewHolder{
        ImageView iv;
        TextView tv;
    }

}
